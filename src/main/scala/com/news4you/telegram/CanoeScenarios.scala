package com.news4you.telegram

import canoe.api.{TelegramClient => Client, _}
import com.news4you.crawler.Crawler
import com.news4you.crawler.Crawler.Crawler
import com.news4you.subscription.Subscription
import com.news4you.subscription.Subscription.Subscription
import zio._

object CanoeScenarios {
    type CanoeScenarios = Has[Service]

    trait Service {
        def news: Scenario[Task, Unit]

        def addSubscription: Scenario[Task, Unit]

        def getSubscriptions: Scenario[Task, Unit]
    }

    type LiveDeps = Has[Client[Task]] with Crawler with Subscription

    def live: URLayer[LiveDeps, Has[Service]] =
        ZLayer.fromServices[Client[Task], Crawler.Service, Subscription.Service, Service] {
            (client, httpClient, subscription) =>
                CanoeScenariosServiceImpl(httpClient, subscription, client)
        }
}
