package com.news4you.telegram

import derevo.derive
import derevo.pureconfig.config

@derive(config)
case class BotConfig(token: String)
