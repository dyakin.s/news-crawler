package com.news4you.telegram

import canoe.api._
import canoe.models.Chat
import canoe.syntax._
import com.news4you.crawler.Crawler
import com.news4you.crawler.Crawler.Document
import com.news4you.subscription.Subscription
import zio.Task

private[telegram] final case class CanoeScenariosServiceImpl(crawler: Crawler.Service,
                                                             subscription: Subscription.Service,
                                                             canoeClient: TelegramClient[Task]
                                                            ) extends CanoeScenarios.Service {

    private implicit val client: TelegramClient[Task] = canoeClient

    override def getSubscriptions: Scenario[Task, Unit] = for {
        (chat, userId) <- Scenario.expect(command("subs")).map(m => (m.chat, m.from.get.id))
        subscriptions <- Scenario.eval(subscription.getSubscriptions(userId))
        _ <- subscriptions match {
            case Nil => Scenario.eval(chat.send("You don't have any subscriptions"))
            case subs => for {
                _ <- Scenario.eval(chat.send(s"Your subscriptions"))
                _ <- Scenario.eval(chat.send(subs.mkString("", "\n", "")))
            } yield ()
        }
    } yield ()

    override def addSubscription: Scenario[Task, Unit] = {
        for {
            (chat, userId) <- Scenario.expect(command("add")).map(m => (m.chat, m.from.get.id))
            _ <- Scenario.eval(chat.send("Where do you want to get the news from"))
            link <- Scenario.expect(text)
            _ <- Scenario.eval(subscription.addSubscription(userId, link))
            _ <- Scenario.eval(chat.send(s"We added $link to your subscription"))
        } yield ()

    }

    override def news: Scenario[Task, Unit] = {
        def provideNews(chat: Chat, links: List[String]): Scenario[Task, Unit] = {
            def formatNews(doc: Document) =
                s"""
                   |${doc.link}
                   |to read ${doc.duration} mins
                   |length news ${doc.words}
                   |""".stripMargin

            for {
                result <- Scenario.eval(crawler.news(links)).attempt
                _ <- result.fold(
                    _ => errorScenario(chat),
                    docs => for {
                        _ <- Scenario.eval(chat.send("Your news:"))
                        _ <-  {
                            def go(docs: List[Document]): Scenario[Task, Unit] = {
                                docs match {
                                    case Nil =>Scenario.done
                                    case d :: tail =>
                                        Scenario.eval(chat.send(formatNews(d)))>>go(tail)
                                }
                            }
                            go(docs)
                        }
                    } yield ()
                )
            } yield ()
        }

        for {
            (chat, userId) <- Scenario.expect(command("news")).map(m => (m.chat, m.from.get.id))
            links <- Scenario.eval(subscription.getSubscriptions(userId))
            _ <- provideNews(chat, links)
        } yield ()
    }

    def errorScenario(chat: Chat) =
        Scenario.eval(chat.send("Something went wrong while making your order. Please try again."))
}
