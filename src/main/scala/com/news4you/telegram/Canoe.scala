package com.news4you.telegram

import canoe.api.{TelegramClient => Client, _}
import com.news4you.config.Config
import zio.interop.catz._
import zio.logging.Logger
import zio.{Has, Task, ZIO, ZLayer}

private[telegram] final case class Canoe(logger: Logger[String],
                                         scenarios: CanoeScenarios.Service,
                                         canoeClient: Client[Task]
                                        ) extends TelegramClient.Service {

    implicit val canoe: Client[Task] = canoeClient

    override def start: Task[Unit] =
        logger.info("Starting Telegram polling") *>
            Bot.polling[Task]
                .follow(
                    scenarios.news,
                    scenarios.addSubscription,
                    scenarios.getSubscriptions
                )
                .compile
                .drain
                .catchAllCause { cause =>
                    logger.error("Error during Bot Scenario", cause)
                }
}

object Canoe {
    def live: ZLayer[Config, Throwable, Has[Client[Task]]] = {
        ZLayer.fromManaged(ZIO.runtime[Any]
            .map { implicit rts =>
                for {
                    token <- ZIO.access[Config](_.get.botConfig.token)
                } yield Client.global[Task](token).toManaged
            }.flatten.toManaged_.flatten)
    }

}
