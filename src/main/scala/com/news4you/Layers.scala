package com.news4you

import canoe.api
import com.news4you.config.Config
import com.news4you.crawler.Crawler
import com.news4you.db.DB
import com.news4you.http.HttpClient
import com.news4you.http.HttpClient.{ClientTask, HttpClient}
import com.news4you.subscription.Subscription
import com.news4you.telegram.TelegramClient.TelegramClient
import com.news4you.telegram.{Canoe, CanoeScenarios, TelegramClient}
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import zio._
import zio.blocking.Blocking
import zio.clock.Clock
import zio.interop.catz._
import zio.logging.Logging
import zio.logging.slf4j.Slf4jLogger

import scala.concurrent.ExecutionContext.Implicits

object Layers {
    type ConfigurationEnv = Config with Logging with Clock with Blocking
    type AppConfigurationEnv = ConfigurationEnv  with ClientTask
    type News4YouEnv = AppConfigurationEnv with HttpClient with TelegramClient

    object live {
        private def makeHttpClient =
            ZIO.runtime[Any].map { implicit rts =>
                BlazeClientBuilder
                    .apply[Task](Implicits.global)
                    .resource
                    .toManaged
            }

        val configurationEnv: ZLayer[Blocking, Throwable, ConfigurationEnv] =
            Blocking.any ++ Clock.live ++ NewsBotConfig.live ++ Slf4jLogger.make((_, msg) => msg)

        val httpEnv: ZLayer[Any, Throwable, Has[Client[Task]]] = ZLayer.fromManaged(makeHttpClient.toManaged_.flatten)

        val appConfigurationEnv: ZLayer[ConfigurationEnv, Throwable, AppConfigurationEnv] =
            httpEnv ++ ZLayer.identity

        val httpClientEnv: ZLayer[Blocking with ConfigurationEnv, Throwable, HttpClient] =
            (configurationEnv ++ appConfigurationEnv) >>> HttpClient.http4s

        val canoeEnv: ZLayer[Config, Throwable, Has[api.TelegramClient[Task]]] =
            Canoe.live

        val canoeScenarioEnv: ZLayer[ConfigurationEnv, Throwable, Has[CanoeScenarios.Service]] =
            (canoeEnv ++ (httpClientEnv >>> Crawler.live) ++ (DB.live >>> Subscription.live)) >>> CanoeScenarios.live

        val telegramClientEnv: ZLayer[ConfigurationEnv, Throwable, Has[TelegramClient.Service]] =
            (configurationEnv ++ canoeScenarioEnv ++ canoeEnv) >>> TelegramClient.canoe

        val appLayer: ZLayer[Blocking, Throwable, News4YouEnv] =
            configurationEnv >>> appConfigurationEnv ++ httpClientEnv ++ telegramClientEnv
    }

}
