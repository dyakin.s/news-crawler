package com.news4you

import com.news4you.config.Config
import com.news4you.db.FlywayMigration
import com.news4you.telegram.TelegramClient.TelegramClient
import zio._
import zio.console.putStrLn

object Server extends App {
    override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] = {
        val prog =
            for {
                cfg <- ZIO.access[Config](_.get)
                _ <- logging.log.info(s"Starting with $cfg")
                _ <- FlywayMigration.migrate(cfg.dbConfig)
                canoe <- ZIO.access[TelegramClient](_.get)
                _ <- canoe.start
            } yield 0

        prog.provideLayer(Layers.live.appLayer).foldM(
            err => putStrLn(s"Execution failed with: ${err.getMessage}") *> ZIO.succeed(1),
            _ => ZIO.succeed(0)
        )
    }
}
