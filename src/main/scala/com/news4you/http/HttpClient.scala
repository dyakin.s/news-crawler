package com.news4you.http

import com.news4you.NewsBotConfig
import com.news4you.config.Config
import io.circe.Decoder
import org.http4s.client.Client
import zio._
import zio.logging.{Logger, Logging}

object HttpClient {
    type HttpClient = Has[Service]

    type ClientTask = Has[Client[Task]]

    trait Service {
        def rootUrl: String

        def get[T](uri: String, parameters: Map[String, String])
                  (implicit d: Decoder[T]): Task[T]

        def get(uri: String): Task[String]
    }

    def http4s: URLayer[Config with Logging with ClientTask, HttpClient] =
        ZLayer.fromServices[NewsBotConfig, Logger[String], Client[Task], Service] { (config, logger, http4sClient) =>
            Http4s(config.news4YouConfig.url, logger, http4sClient)
        }
}