package com.news4you.http

import derevo.derive
import derevo.pureconfig.config

@derive(config)
case class HttpConfig(url: String)
