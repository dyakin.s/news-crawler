package com.news4you

import com.news4you.config.Config
import com.news4you.db.DBConfig
import com.news4you.http.HttpConfig
import com.news4you.telegram.BotConfig
import derevo.derive
import derevo.pureconfig.config
import pureconfig.ConfigSource
import zio.blocking.{Blocking, effectBlocking}
import zio.{ZLayer, _}

@derive(config)
case class NewsBotConfig(news4YouConfig: HttpConfig,
                         botConfig: BotConfig,
                         dbConfig: DBConfig)

object NewsBotConfig {
    val live: RLayer[Blocking, Config] =
        ZLayer.fromEffect(effectBlocking(ConfigSource.default.loadOrThrow[NewsBotConfig]))

}
