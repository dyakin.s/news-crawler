package com.news4you.crawler

import com.news4you.crawler.Crawler.{Document, Service}
import com.news4you.http.HttpClient
import org.jsoup.Jsoup
import zio.{Task, _}

import scala.jdk.CollectionConverters._

private[crawler] final case class CrawlerImpl(httpClient: HttpClient.Service) extends Service {
    override def news(resources: List[String]): Task[List[Document]] = {
        def resourceNews(resource:String) =
            httpClient.get(resource).flatMap { response =>
                ZIO.foreachPar(Jsoup.parse(response).select("a").asScala
                    .filter(e => e.hasAttr("href") && e.attr("href").exists(_.isDigit))
                    .take(3)) { newElm =>
                    val link = newElm.attr("href")
                    val newsResource = if (link.contains("http")) link else resource + link
                    httpClient.get(newsResource).map { news =>
                        val words = Jsoup.parse(news).text().split("[,\\.\\s+]").length
                        Document(newsResource, words / 160, words)
                    }
                }
            }

        ZIO.foreach(resources)(resourceNews).map(_.flatten)
    }
}
