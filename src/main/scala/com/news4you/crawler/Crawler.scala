package com.news4you.crawler

import com.news4you.http.HttpClient
import com.news4you.http.HttpClient.HttpClient
import zio._

object Crawler {

    case class Document(link: String, duration: Int, words: Int)

    type Crawler = Has[Service]

    trait Service {
        def news(links: List[String]): Task[List[Document]]
    }

    val live: ZLayer[HttpClient, Nothing, Crawler] =
        ZLayer.fromService[HttpClient.Service, Service] { http4sClient =>
            CrawlerImpl(http4sClient)
        }

}
