package com.news4you

import zio.Has

package object db {
  type DB = Has[DB.Service]
}
