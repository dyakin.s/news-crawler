package com.news4you.db

import cats.effect.Blocker
import com.news4you.config.Config
import doobie.h2.H2Transactor
import doobie.util.transactor.Transactor
import zio._
import zio.blocking.{Blocking, blocking}
import zio.interop.catz._

object DB {
  case class Service(transactor: Transactor[Task])

  val live: RLayer[Config with Blocking, DB] =
    ZLayer.fromManaged(
      for {
        liveEC  <- ZIO.descriptor.map(_.executor.asEC).toManaged_
        blockEC <- blocking(ZIO.descriptor.map(_.executor.asEC)).toManaged_
        conf    <- RIO.access[Config](_.get.dbConfig).toManaged_
        trans <- H2Transactor
                  .newH2Transactor[Task](
                    conf.url,
                    conf.user,
                    conf.password,
                    liveEC,
                    Blocker.liftExecutionContext(blockEC)
                  )
                  .toManagedZIO
      } yield Service(trans)
    )
}
