package com.news4you

import zio.Has

package object config {
   type Config = Has[NewsBotConfig]
}