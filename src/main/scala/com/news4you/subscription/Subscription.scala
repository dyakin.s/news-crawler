package com.news4you.subscription

import com.news4you.db.DB
import zio._

object Subscription {
    type Subscription = Has[Service]

    trait Service {
        def addSubscription(userId: Int, subscription: String): Task[Unit]

        def getSubscriptions(userId: Int): Task[List[String]]
    }

    val live: ZLayer[DB, Nothing, Has[Service]] =
        ZLayer.fromEffect(ZIO.access[DB](xa => Doobie(xa.get.transactor)))

}
