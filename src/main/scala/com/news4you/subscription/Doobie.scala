package com.news4you.subscription

import com.news4you.subscription.Subscription.Service
import doobie.implicits._
import doobie.util.transactor.Transactor
import doobie.{Query0, Update0}
import zio.Task
import zio.interop.catz._

private[subscription] final case class Doobie(xa: Transactor[Task]) extends Service {

  override def addSubscription(userId: Int,subscription:String): Task[Unit] =
    SQL
      .addSubscription(userId,subscription)
      .run
      .transact(xa)
      .unit
      .orDie

  override def getSubscriptions(userId: Int): Task[List[String]] =
  SQL
      .getSubscriptions(userId)
      .to[List]
      .transact(xa)
      .orDie
}

private object SQL {
  type Subscriptions = List[String]

  def addSubscription(userId:Int,subscription:String): Update0 =
    sql"""INSERT INTO subscriptions (id,subscription) VALUES ($userId,$subscription)""".update

  def getSubscriptions(userId:Int): Query0[String] =
    sql"""SELECT subscription FROM subscriptions where $userId""".query[String]
}
