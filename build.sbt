val DoobieVersion     = "0.8.6"
val ZIOVersion        = "1.0.0-RC18-2"
val ZIOLoggingVersion = "0.2.7"
val SilencerVersion   = "1.4.4"
val Log4j2Version     = "2.13.1"
val derevoVersion     = "0.11.2"

addCommandAlias("build", "prepare; test")
addCommandAlias("prepare", "fix; fmt")
addCommandAlias("check", "fixCheck; fmtCheck")
addCommandAlias("fix", "all compile:scalafix test:scalafix")
addCommandAlias(
  "fixCheck",
  "compile:scalafix --check; test:scalafix --check"
)
addCommandAlias("fmt", "all scalafmtSbt scalafmt test:scalafmt")
addCommandAlias(
  "fmtCheck",
  "all scalafmtSbtCheck scalafmtCheck test:scalafmtCheck"
)

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging, DockerSpotifyClientPlugin)
  .settings(
    packageName in Docker := "zio-news4you-bot",
    name := "news4youbot",
    scalaVersion := "2.13.1",
    scalacOptions := Seq(
      "-feature",
      "-deprecation",
      "-explaintypes",
      "-unchecked",
      "-Ymacro-annotations",
      "-encoding",
      "UTF-8",
      "-language:higherKinds",
      "-language:existentials",
      "-Xfatal-warnings",
      "-Xlint:-infer-any,_",
      "-Ywarn-value-discard",
      "-Ywarn-numeric-widen",
      "-Ywarn-extra-implicit",
      "-Ywarn-unused:_"
    ) ++ (if (isSnapshot.value) Seq.empty
          else
            Seq(
              "-opt:l:inline"
            )),
    libraryDependencies ++= Seq(
      "org.tpolecat"             %% "doobie-core"         % DoobieVersion,
      "org.tpolecat"             %% "doobie-h2"           % DoobieVersion,
      "org.tpolecat"             %% "doobie-hikari"       % DoobieVersion,
      "dev.zio"                  %% "zio"                 % ZIOVersion,
      "dev.zio"                  %% "zio-test"            % ZIOVersion % "test",
      "dev.zio"                  %% "zio-test-sbt"        % ZIOVersion % "test",
      "dev.zio"                  %% "zio-interop-cats"    % "2.0.0.0-RC12",
      "dev.zio"                  %% "zio-logging"         % ZIOLoggingVersion,
      "dev.zio"                  %% "zio-logging-slf4j"   % ZIOLoggingVersion,
      "org.flywaydb"             % "flyway-core"          % "5.2.4",
      "com.h2database"           % "h2"                   % "1.4.199",
      "org.apache.logging.log4j" % "log4j-api"            % Log4j2Version,
      "org.apache.logging.log4j" % "log4j-core"           % Log4j2Version,
      "org.apache.logging.log4j" % "log4j-slf4j-impl"     % Log4j2Version,
      "com.github.pureconfig"    %% "pureconfig"          % "0.12.1",
      "com.lihaoyi"              %% "sourcecode"          % "0.1.7",
      "org.jsoup"                % "jsoup"                 % "1.13.1",
      "org.augustjune"           %% "canoe"                % "0.4.1",
      "org.manatki"                  %% "derevo-cats"                   % derevoVersion,
      "org.manatki"                  %% "derevo-pureconfig"             % derevoVersion,
      "org.manatki"                  %% "derevo-circe"                  % derevoVersion,
      ("com.github.ghik" % "silencer-lib" % SilencerVersion % "provided")
        .cross(CrossVersion.full),
      // plugins
      compilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
      compilerPlugin(
        ("org.typelevel" % "kind-projector" % "0.11.0").cross(CrossVersion.full)
      ),
      compilerPlugin(
        ("com.github.ghik" % "silencer-plugin" % SilencerVersion)
          .cross(CrossVersion.full)
      ),
      compilerPlugin(scalafixSemanticdb)
    )
  )
