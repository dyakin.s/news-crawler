A bot that allows you to add subscriptions to various information sources and get the latest news

contributor: Sergey Dyakin (@dyakin.s)

Stack for 🤖: 
Scala, http4s, Zio, Canoe, Doobie, Flyway , Cats

🤖 commands:
- /add - add new subscription
- /subs - get all subscriptions
- /news - get actual news for all subscriptions


